from django.contrib import admin
from todos.models import TodoList, TodoItem


# Register your models here.
@admin.register(TodoList)
class TodosListAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(TodoItem)
class TodosItemAdmin(admin.ModelAdmin):
    list_display = (
        "task",
        "due_date",
    )
